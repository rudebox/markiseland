<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	$page_img = get_field('page_img') ? : $page_img = get_field('page_img', 'options');
?>

<?php if ($page_img && is_singular('produkt') || is_page_template('product-teaser.php') || is_404() ) : ?>
	<div class="page__img" style="background-image: url(<?php echo esc_url($page_img['url']); ?>);">
		
	</div>
<?php endif; ?>

<?php if (!is_singular('produkt') && !is_page_template('product-teaser.php') ) : ?>

	<section class="page__hero">
		<div class="wrap hpad">
			<h1 class="page__title"><?php echo esc_html($title); ?></h1>
		</div>
	</section>

<?php endif; ?>