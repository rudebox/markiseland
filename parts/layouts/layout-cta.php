<?php 
/**
* Description: Lionlab shortcode repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

//fields

$bg = get_sub_field('cta_bg');
$text = get_sub_field('cta_text');
$title = get_sub_field('cta_title');


?>

<section class="cta cta--loan padding--<?php echo esc_attr($margin); ?>" style="background-image: url(<?php echo esc_url($bg['url']); ?>);">
	<div class="wrap hpad">
		<div class="row">

			<div class="col-sm-6 cta__text">
				<h2><?php echo esc_html($title); ?></h2>
				<?php echo $text; ?>
			</div>

		</div>
	</div>
</section>