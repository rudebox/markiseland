<?php 
/**
* Description: Lionlab selection field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');


$title = get_sub_field('header');
$text = get_sub_field('text');
$circle_text = get_sub_field('circle_tekst');
$img = get_sub_field('bg_img');
$link = get_sub_field('link');
?>

<section class="selection padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="flex flex--wrap">

			<div class="col-sm-6 selection__item selection__item--text bg--<?php echo esc_attr($bg); ?>">
				<h2 class="selection__title"><?php echo esc_html($title); ?></h2>
				<?php echo $text; ?>
				<a class="selection__link" href="<?php echo esc_url($link); ?>">Se mere her <i class="fas fa-angle-right"></i></a>
			</div>

			<div class="col-sm-6 selection__item selection__item--img"  style="background-image: url(<?php echo esc_url($img['url']); ?>);">
				<a class="selection__circle" href="<?php echo esc_url($link); ?>">
					<div class="selection__wrap">
						<h2 class="selection__title--circle"><?php echo esc_html($circle_text); ?></h2>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tilde_yellow.png" alt="tilde">
					</div>
				</a>
			</div>
			
		</div>
	</div>
</section>
