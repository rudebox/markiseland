<?php 
/**
* Description: Lionlab mosiac gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//img
$img = get_sub_field('mosaic_gallery'); 

//counter
$i=0;

//text
$title = get_sub_field('mosaic_title');
$text = get_sub_field('mosaic_text');
?>

<section class="mosaic">
	<div class="wrap hpad">

		<div class="mosaic__row flex flex--wrap">

			<div class="col-sm-6 mosaic__cta" style="background-image: url(<?php echo esc_url($img['url']); ?>);">

				<?php echo do_shortcode('[gardinbus_cta]'); ?>


			</div>

			<div class="col-sm-6 mosaic__text">
				<h2 class="mosaic__title"><?php echo $title; ?></h2>
				<?php echo $text; ?>
			</div>

		</div>
	</div>
</section>