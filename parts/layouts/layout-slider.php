<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

if ( have_rows('slides') ) : ?>

  <section class="slider">
    <div class="slider__track is-slider">

      <?php
        // Loop through slides
        while ( have_rows('slides') ) :
          the_row();
        $image   = get_sub_field('slides_bg');
        $title = get_sub_field('slides_title');
        $caption = get_sub_field('slides_text'); 
        $link = get_sub_field('slides_link'); 
        $link_text = get_sub_field('slides_link_text');
      ?>

        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
          <div class="wrap hpad slider__container">
            <div class="slider__text col-sm-6 col-sm-offset-6">
              <h2 class="slider__title"><?php echo esc_html($title); ?></h2>
              <?php echo $caption; ?>
              <?php if ($link) : ?>
                <a class="slider__link btn btn--red" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
              <?php endif; ?>
            </div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>
  </section>
<?php endif; ?>