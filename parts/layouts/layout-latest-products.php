<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

//section settings
$margin = get_sub_field('margin');

if ( have_rows('product_slides') ) : ?>

  <section class="slider slider--latest-products padding--<?php echo esc_attr($margin); ?>">
    <div class="wrap hpad slider__container">
      <div class="slider__track--latest-products is-slider">

        <?php
          // Loop through slides
          while ( have_rows('product_slides') ) :
            the_row();
          $image   = get_sub_field('product_slides_bg');
          $title = get_sub_field('product_slides_title');
          $caption = get_sub_field('product_slides_text'); 
          $link = get_sub_field('product_slides_link'); 
          $link_text = get_sub_field('product_slides_link_text');
          $link_target = get_sub_field('product_slides_link_target');
        ?>

          <div class="slider__item flex flex--valign"  style="background-image: url(<?php echo esc_url($image['url']); ?>);">
            
              <div class="slider__text slider__text--latest-products col-sm-6 col-sm-offset-6">
                <p>Mest solgte varer</p>
                <h2 class="slider__title"><?php echo esc_html($title); ?></h2>
                <?php echo $caption; ?>
                <?php if ($link && $link_text) : ?>
                  <?php if ($link_target === false) : ?>
                    <a class="slider__link" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?> <i class="fas fa-angle-right"></i></a>
                  <?php else : ?>
                    <a target="_blank" rel="noopener" class="slider__link no-ajax" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?> <i class="fas fa-angle-right"></i></a>
                  <?php endif; ?>
                <?php endif; ?>
              </div>
            
          </div>

        <?php endwhile; ?>

      </div>
    </div>
  </section>
<?php endif; ?>