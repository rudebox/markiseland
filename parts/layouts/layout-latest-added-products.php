<?php 
  //sections settings
  $margin = get_sub_field('margin');
?>

 <section class="products padding--<?php echo esc_attr($margin); ?>">

    <div class="wrap hpad">
      <h2 class="products__header center">Udvalgte produkter</h2>
      <div class="row flex flex--wrap">
        
          <?php 

            //query arguments
            $args = array(
              'posts_per_page' => 6,
              'post_type' => 'produkt',
              'orderby' => 'date'
            );
             
            $query = new WP_QUERY($args);
           ?>

          <?php if ($query->have_posts()): ?>
            <?php while ($query->have_posts()): $query->the_post(); ?>

            <?php   
              //get thumb
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'products' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

              //product sales badge
              $sales_badge = get_field('product_sales_badge');

              if ($sales_badge === true) {
                $sales_badge_class = 'products__item--sales-badge';
              }

            ?>

             <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="products__item col-sm-4 <?php echo esc_attr($sales_badge_class); ?> has-btn" itemscope itemtype="http://schema.org/Product">

                <header>
                  <?php if ($sales_badge === true)  : ?>
                    <div class="products__sales-badge">Tilbud</div>
                  <?php endif; ?>
                  <img itemprop="image" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">
                  <h2 class="products__title--archive" itemprop="name">                   
                      <?php the_title(); ?>
                  </h2>
                </header>

                <div itemprop="description">
                  <?php the_excerpt(); ?>
                  <span class="btn btn--red products__btn">Se mere</span>
                </div>

              </a>

            <?php endwhile; wp_reset_postdata(); else: ?>

              <p>Ingen produkter i denne kategori fundet.</p>

          <?php endif; ?>

      </div>
    </div>
  </section>