<form class="search__form" method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
	<input class="search__input" type="text" value="<?php the_search_query(); ?>" placeholder="<?php _e('Søg varer her...', 'lionlab'); ?>" name="s" id="s"></input> 
</form>