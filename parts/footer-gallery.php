<?php 
/**
* Description: Lionlab gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//gallery
$gallery = get_field('gallery', 'options'); 
$index = get_row_index();


if ( $gallery ) : ?> 

	<section class="gallery" itemscope itemtype="ImageGallery">
		<div class="wrap--fluid">

			<div class="gallery__list flex flex--wrap">

				<?php
					// Loop through gallery
					foreach ( $gallery as $image ) : 
				?>


					<div class="gallery__item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
						
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</section>

<?php endif; ?>