<?php

	$gallery = get_field('product_gallery');

	//get thumb
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'products' );
	$large = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
	//post img alt tag
	$alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

	$index = get_row_index();

?>

    <figure class="product-gallery__item product-gallery__item--featured" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
			<a href="<?= $large[0]; ?>" class="js-zoom product-gallery__link no-ajax" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl" title="<?= $thumb['title'] ?>" data-width="<?= $thumb['sizes']['large-width']; ?>" data-height="<?= $thumb['sizes']['large-height']; ?>">

				<img class="product-gallery__image" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">
			</a>
	</figure>

<?php if ( $gallery ) : ?>

	<section class="product-gallery" itemscope itemtype="http://schema.org/ImageGallery">
		<div class="wrap">

			<div class="row flex flex--wrap product-gallery__list">

				<?php
				// Loop through gallery
				foreach ( $gallery as $image ) : ?>

					<figure class="product-gallery__item col-xs-4 col-md-3" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
						<a href="<?= $image['sizes']['large']; ?>" class="js-zoom product-gallery__link no-ajax" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl" title="<?= $image['title'] ?>" data-width="<?= $image['sizes']['large-width']; ?>" data-height="<?= $image['sizes']['large-height']; ?>">
							<img class="product-gallery__image" src="<?= $image['sizes']['products']; ?>" alt="<?= $image['alt']; ?>" itemprop="thumbnail" height="<?= $image['sizes']['large-height']; ?>" width="<?= $image['sizes']['large-width']; ?>">
						</a>
					</figure>

				<?php endforeach; ?>

			</div>
		</div>
	</section>

<?php endif; ?>
