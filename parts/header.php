<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVJ84PB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<?php 
  $phone = get_field('phone', 'options');
  $mail = get_field('mail', 'options');
  $open_hours = get_field('open_hours', 'options');
  $facebook = get_field('facebook', 'options');
  $instagram = get_field('instagram', 'options');
 ?>

<div class="toolbar">
  <div class="wrap hpad flex flex--center flex--justify">
    <div class="toolbar__item">
      <p>RING <a href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a> <?php echo esc_html($open_hours); ?></p>
    </div>
    <div class="toolbar__item">
      <a rel="noopener" href="mailto:<?php echo $mail; ?>"><?php echo esc_html($mail); ?></a> <?php if ($facebook) : ?><a target="_blank" href="<?php echo esc_url($facebook); ?>"><i class="fab fa-facebook-f"></i></a> <?php endif; ?> <?php if ($instagram) : ?><a rel="noopener" target="_blank" href="<?php echo esc_url($instagram); ?>"><i class="fab fa-instagram"></i></a><?php endif; ?>
    </div>
  </div>
</div>

<header class="header" id="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="wrap hpad flex flex--hvalign header__container header__container--logo">

    <?php 
      //logo SEO markup
      $site_name = get_bloginfo( 'name' );
      $logo_markup  = ( is_front_page() ) ? '<h1 class="visuallyhidden">' . $site_name . '</h1>' : '<p class="visuallyhidden">' . $site_name . '</p>';
    ?>

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/markiseland_logo.png" alt="<?php bloginfo('name'); ?>">
      <?php echo $logo_markup; ?>
    </a>

  </div>

  <div class="wrap hpad flex flex--center flex--justify header__container header__container--nav">

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
        <?php get_template_part('parts/search-bar'); ?>
      </div>
    </nav>

  </div>
</header>

<?php 
  $icon = get_field('contact_icon', 'options'); 
  $link = get_field('contact_link', 'options'); 

  $bus_icon = get_field('bus_icon', 'options'); 
  $bus_link = get_field('bus_link', 'options'); 
?>

<a href="<?php echo esc_url($link); ?>" class="contact__icon contact__icon--chat">
    <?php echo $icon; ?>
</a>

<a href="<?php echo esc_url($bus_link); ?>" class="contact__icon contact__icon--bus">
    <?php echo $bus_icon; ?>
</a>

<div id="transition" class="transition">

<div class="transition__container" data-namespace="general"> 
