<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<?php 
	//contact fields
	$title = get_field('contact_title'); 
	$text = get_field('contact_text'); 
	$cta = get_field('contact_cta'); 
?>

<?php get_template_part('parts/product', 'slider');?>

<main>

	<?php get_template_part('parts/page', 'header');?>
	
	<section class="contact padding--bottom">
		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="col-sm-6 contact__form">
								
					<?php echo $text; ?>
					<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>

				<div class="col-sm-6 contact__cta">		
					<?php echo $cta; ?>
				</div>

			</div>
		</div>
	</section>

	<?php get_template_part('parts/footer', 'gallery'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
