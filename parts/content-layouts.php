<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'slider' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    } elseif( get_row_layout() === 'latest-products' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'latest-products' ); ?>

    <?php
    } elseif( get_row_layout() === 'cta' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'cta' ); ?>

    <?php
    } elseif( get_row_layout() === 'mosaic-gallery' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'mosaic-gallery' ); ?>

    <?php
    } elseif( get_row_layout() === 'latest-added-products' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'latest-added-products' ); ?>

    <?php
    } elseif( get_row_layout() === 'selection' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'selection' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
