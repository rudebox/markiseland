<aside class="sidebar">
	 <?php 
        $category_name = "montering";
        $category = get_category_by_slug( $category_name );
        $category_id = $category->term_id;

        $args = array('child_of' => $category_id);
        $categories = get_categories( $args );
	  ?>
		
		<div class="sidebar__category">
			<h6 class="sidebar__title" data-target="<?php echo $category->name; ?>" id="<?php echo $category->name; ?>"><?php echo $category->name; ?> <i class="sidebar__icon"></i></h6>

			<div class="sidebar__wrap" id="<?php echo $category->name; ?>">
				<label for="Alle"><input type="radio" name="filter" data-filter="all" class="sidebar__checkbox">Alle</input></label>

				<?php foreach ($categories as $category) :  ?>
				<label class="sidebar__label" for="<?php echo $category->name; ?>">
				    <input class="sidebar__checkbox" data-filter=".cat<?php echo $category->term_id;?>" type="radio" id="<?php echo $category->name; ?>" name="filter" value="<?php echo $category->name; ?>"></input>
				    <?php echo $category->name; ?>
				</label>
				<?php endforeach; ?>
			</div>
		</div>
</aside>