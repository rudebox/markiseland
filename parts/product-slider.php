<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

// get the current taxonomy term
$term = get_queried_object();

if ( have_rows('slides', $term) ) : ?>

  <section class="slider">
    <div class="slider__track is-slider">

      <?php
        // Loop through slides
        while ( have_rows('slides', $term) ) :
          the_row();
        $image   = get_sub_field('slides_bg') ? : $image = get_sub_field('slides_bg', $term);
        $title = get_sub_field('slides_title') ? : $title = get_sub_field('slides_title', $term);
        $caption = get_sub_field('slides_text') ? : $caption = get_sub_field('slides_text', $term); 
        $link = get_sub_field('slides_link') ? : $link = get_sub_field('slides_link', $term); 
        $link_text = get_sub_field('slides_link_text') ? : $link_text = get_sub_field('slides_link_text', $term);
        $link_target = get_sub_field('slides_link_target') ? : $link_target = get_sub_field('slides_link_target', $term);
      ?>

        <div class="slider__item flex flex--valign"  style="background-image: url(<?php echo esc_url($image['url']); ?>);">
          <div class="wrap hpad slider__container">
            <div class="slider__text col-sm-6 col-sm-offset-6">
              <h2 class="slider__title h1"><?php echo $title; ?></h2>
              <?php echo $caption; ?>
              <?php if ($link && $link_text) : ?>
                <?php if ($link_target === false) : ?>
                <a class="slider__link btn btn--red" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
                <?php else : ?>
                <a target="_blank" rel="noopener" class="slider__link btn btn--red no-ajax" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
                <?php endif; ?>  
              <?php endif; ?>
            </div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>
  </section>
<?php endif; ?>