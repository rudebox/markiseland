<?php get_template_part('parts/header'); the_post(); ?>

<?php get_template_part('parts/page', 'header'); ?>

<main>

  <section class="products padding--both">
    <div class="wrap hpad">

      <article class="products__content">
        
        <div class="row flex flex--wrap products__row">
          <div class="col-sm-6 products__info">

            <header>
              <h1 class="page__title products__title">
                <?php the_title(); ?>
              </h1>
            </header>

            <div class="products__text">
              <?php the_content(); ?>              
            </div>

            <div class="products__form">
               <?php $form_id = get_field('product_form_id'); ?>
               <?php if ($form_id) : ?>
                  <div class="gform_heading">
                    <h3 class="gform_title">Få tilsendt tilbud</h3>
                  </div>
                 <?php gravity_form( $form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
               <?php endif; ?>
            </div>

          </div>

          <div class="col-sm-6 products__cta">
            <?php get_template_part('parts/product', 'gallery'); ?>
            <?php echo do_shortcode('[gardinbus_cta]'); ?>
          </div>

        </div>

      </article>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer', 'gallery'); ?>

<?php get_template_part('parts/footer'); ?>