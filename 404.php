<?php get_template_part('parts/header'); ?>

<main class="no-figure">

  <?php get_template_part('parts/page', 'header');?>

  <section class="error padding--bottom">
  	<div class="wrap hpad">

    	<p>Beklager, men siden du søgte efter eksisterer ikke.</p>
		<a class="btn btn--red" href="/">Tilbage til forsiden</a>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>