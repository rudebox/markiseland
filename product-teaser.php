
<?php

/*
 * Template Name: Produkt teaser
 */

get_template_part('parts/header'); the_post(); 

$cta_img = get_field('product_teaser_cta_img');
$cta_title = get_field('product_teaser_cta_title');
$cta_text = get_field('product_teaser_cta_text');
?>


<main>

	<?php get_template_part('parts/page', 'header');?>
	
	<section class="products padding--both">
		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="col-sm-6 products__info">

					<header>
		              <h1 class="page__title products__title">
		                <?php the_title(); ?>
		              </h1>
            		</header>
					
					<div class="products__text">
						<?php the_content(); ?>

						<div class="products__text-assortment">
							OPLEV VORES UDVALG <strong><a href="/produkt">HER</a></strong>
						</div>

						<div class="products__text-offer">
							OG BESTIL <strong><a href="/produkt">TILBUD</a></strong>
						</div>
					</div>

				</div>

				<?php  
					if (have_rows('product_teaser_img') ) :

						//counter
						$i=0;
				?>

				<div class="col-sm-6 products__img">
					
					<?php 
						while (have_rows('product_teaser_img') ) : the_row(); 
							$img = get_sub_field('img');

							$i++;

							if ($i === 2) {
								echo '<div class="products__teaser-wrap flex flex--wrap">';

							}

					?>

						<div class="products__teaser-img products__teaser-img--<?php echo esc_attr($i); ?>" style="background-image: url(<?php echo $img['url']; ?>);">
							
						</div>


						<?php 
							if ($i === 2) {
								echo' <a class="selection__circle" href="/produkt">
										<div class="selection__wrap">
											<h2 class="selection__title--circle">OPLEV VORES UDVALG HER</h2>
											<img src="' . get_template_directory_uri() . '/assets/img/tilde_yellow.png" alt="tilde">
										</div>
									</a>';
								echo '</div>';
							}	 
						?>
					<?php endwhile; ?>

				</div>

				<?php endif; ?>

			</div>
		</div>
	</section>

	<section class="products padding--bottom">
		<div class="wrap hpad">
			<div class="row flex flex--center flex--wrap">
				
				<div class="col-sm-6 products__teaser-cta">
					<?php echo do_shortcode('[gardinbus_cta]'); ?>
				</div>
				
				<div class="col-sm-6 cta--loan">
						<img src="<?php echo esc_url($cta_img['sizes']['cta']); ?>" alt="<?php echo esc_attr($cta_img['alt']); ?>">
						<h2 class="cta__title cta__title--product-teaser"><?php echo esc_html($cta_title); ?></h2>
						<?php echo $cta_text; ?>
				</div>

			</div>
		</div>
	</section>

	<?php get_template_part('parts/footer', 'gallery'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
