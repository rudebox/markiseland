<?php

/*
 * Template Name: Layouts
 */

get_template_part('parts/header'); the_post(); ?>

<?php get_template_part('parts/product', 'slider');?>

<main>
	
	<?php get_template_part('parts/page', 'header');?>

	<?php get_template_part('parts/content', 'layouts'); ?>

</main>

<?php get_template_part('parts/footer', 'gallery'); ?>

<?php get_template_part('parts/footer'); ?>
