<?php
 /**
   * Description: Lionlab helpers
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	//remove countrycode from phone
	function get_formatted_phone($str) {
	  
	  // Remove +45
	  $str = str_replace('+', '00', $str);

	  // Only allow integers
	  $str = preg_replace('/[^0-9]/s', '', $str);

	  return $str;
	}


	//allow svg uploads
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');


	// Custom excerpt length
	function custom_excerpt_length( $length ) {
	  return 20;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// Custom excerpt text
	function custom_excerpt_more( $more ) {
	  return '&hellip;';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');


	// Move Yoast to bottom
	function yoasttobottom() {
	  return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


	//get proper title
	function get_proper_title( $id = null  ) {
	  
	  if ( is_null($id) ) {
	    global $post;
	    $id = $post->ID;
	  }

	  $acf_title = get_field('page_title', $id);

	  return ($acf_title) ? $acf_title : get_the_title( $id );
	}


	//force categories of custom post type use default archive too
	function lionlab_show_cpt_archives( $query ) {

	 if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
	   $query->set( 'post_type', array(
	   'post', 'nav_menu_item', 'produkt'
	 ));

	 return $query;
	 }

	}

	add_filter('pre_get_posts', 'lionlab_show_cpt_archives');


	//limit search to products
	function searchfilter($query) {
   
	if ($query->is_search && !is_admin() ) {
	      $query->set('post_type',array('produkt'));
	  }

	  return $query;
	}
	 
	add_filter('pre_get_posts','searchfilter');

?>