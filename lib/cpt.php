<?php 
	// Register Custom Post Type produkt
	function create_produkt_cpt() {

		$labels = array(
			'name' => _x( 'produkter', 'Post Type General Name', 'lionlab' ),
			'singular_name' => _x( 'produkt', 'Post Type Singular Name', 'lionlab' ),
			'menu_name' => _x( 'produkter', 'Admin Menu text', 'lionlab' ),
			'name_admin_bar' => _x( 'produkt', 'Add New on Toolbar', 'lionlab' ),
			'archives' => __( 'produkt Archives', 'lionlab' ),
			'attributes' => __( 'produkt Attributes', 'lionlab' ),
			'parent_item_colon' => __( 'Parent produkt:', 'lionlab' ),
			'all_items' => __( 'Alle produkter', 'lionlab' ),
			'add_new_item' => __( 'Tilføj nyt produkt', 'lionlab' ),
			'add_new' => __( 'Tilføj ny', 'lionlab' ),
			'new_item' => __( 'Ny produkt', 'lionlab' ),
			'edit_item' => __( 'Redigere produkt', 'lionlab' ),
			'update_item' => __( 'Update produkt', 'lionlab' ),
			'view_item' => __( 'Se produkt', 'lionlab' ),
			'view_items' => __( 'Se produkter', 'lionlab' ),
			'search_items' => __( 'Søg produkt', 'lionlab' ),
			'not_found' => __( 'Not found', 'lionlab' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
			'featured_image' => __( 'Featured Image', 'lionlab' ),
			'set_featured_image' => __( 'Set featured image', 'lionlab' ),
			'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
			'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
			'insert_into_item' => __( 'Insert into produkt', 'lionlab' ),
			'uploaded_to_this_item' => __( 'Uploaded to this produkt', 'lionlab' ),
			'items_list' => __( 'produkter list', 'lionlab' ),
			'items_list_navigation' => __( 'produkter list navigation', 'lionlab' ),
			'filter_items_list' => __( 'Filter produkter list', 'lionlab' ),
		);
		$args = array(
			'label' => __( 'produkt', 'lionlab' ),
			'description' => __( '', 'lionlab' ),
			'labels' => $labels,
			'menu_icon' => 'dashicons-cart',
			'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'trackbacks'),
			'taxonomies' => array('category'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 5,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'can_export' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'exclude_from_search' => false,
			'show_in_rest' => true,
			'publicly_queryable' => true,
			'capability_type' => 'post',
		);
		register_post_type( 'produkt', $args );

	}
	add_action( 'init', 'create_produkt_cpt', 0 );
 ?>