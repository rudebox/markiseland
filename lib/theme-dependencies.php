<?php
 /**
   * Description: Lionlab framework
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	$library = array(
		'lionlab-helpers.php',
		'clean-walker.php',
		'clean-up.php',
		'html-compression.php'
	);

	foreach ($library as $file) {

		$filepath = __DIR__ . '/' . $file;

		if ( !file_exists($filepath) ) {
			trigger_error( sprintf('Error locating %s for inclusion', $file), E_USER_ERROR );
		}
		require_once $filepath;
	}

	unset($file, $filepath);

?>
