<?php get_template_part('parts/header'); ?>

<main class="no-figure">

<?php get_template_part('parts/product', 'slider');?>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="products padding--bottom">

    <div class="wrap hpad">

      <div class="products__intro">
        <?php if (category_description() ) : ?>
          <?php echo category_description(); ?>
        <?php endif; ?>
      </div>

      <div class="row flex flex--wrap">

          <?php if (have_posts()): ?>
            <?php while (have_posts()): the_post(); ?>

            <?php   
              //get thumb
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'products' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

              $cats = get_the_category();
              $cat_string = "";

              //product sales badge
              $sales_badge = get_field('product_sales_badge');

              if ($sales_badge === true) {
                $sales_badge = 'products__item--sales-badge';
              }
            ?>

            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="products__item col-sm-4 <?php echo esc_attr($sales_badge); ?>" itemscope itemtype="http://schema.org/Product">

              <header>
                <img itemprop="image" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">
                <h2 class="products__title--archive" itemprop="name">                   
                    <?php the_title(); ?>
                </h2>
              </header>

              <div itemprop="description">
                <?php the_excerpt(); ?>
              </div>

            </a>

            <?php endwhile; else: ?>

              <p>Ingen produkter i denne kategori fundet.</p>

          <?php endif; ?>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer', 'gallery'); ?>

<?php get_template_part('parts/footer'); ?>