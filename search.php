<?php get_template_part('parts/header'); ?>

<main itemscope itemtype="http://schema.org/SearchResultsPage" class="no-figure">

<?php get_template_part('parts/slider');?>

<?php get_template_part('parts/page', 'header'); ?>

<p class="center">
  <strong>Søgeresultat for:</strong>
  <?php echo esc_attr(get_search_query()); ?>
</p>

  <section class="products padding--bottom">

    <div class="wrap hpad">
      <div class="row flex flex--wrap">
    
          <?php 

            //query arguments
            $args = array(
              'post_type' => 'produkt'
            );
             
            $query = new WP_QUERY($args);
          ?>

          <?php if (have_posts()): ?>
            <?php while (have_posts()): the_post(); ?>

            <?php   
              //get thumb
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'products' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

              $cats = get_the_category();
              $cat_string = "";

              //product sales badge
              $sales_badge = get_field('product_sales_badge');

              if ($sales_badge === true) {
                $sales_badge_class = 'products__item--sales-badge';
              }

              foreach ($cats as $cat) {
                $cat_string .= " cat" . $cat->term_id ."";
              }
            ?>

            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="products__item col-sm-4 <?php echo esc_attr($class); ?> <?php echo esc_attr($sales_badge_class); ?> mix <?php echo esc_attr($cat_string); ?>" itemscope itemtype="http://schema.org/BlogPosting">

              <header>
                <?php if ($sales_badge === true)  : ?>
                  <div class="products__sales-badge">Tilbud</div>
                <?php endif; ?>
                <img src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">
                <h2 class="products__title--archive" itemprop="headline">                   
                    <?php the_title(); ?>
                </h2>
              </header>

              <div itemprop="articleBody">
                <?php the_excerpt(); ?>
              </div>

            </a>

            <?php endwhile; else: ?>

              <p><?php _e('Din søgning for', 'lionlab'); ?> <strong><?php echo esc_attr(get_search_query()); ?></strong> <?php _e('gav ingen resultater', 'lionlab'); ?></p>

          <?php endif; ?>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>