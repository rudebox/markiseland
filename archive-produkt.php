<?php get_template_part('parts/header'); ?>

<main class="no-figure">

<?php 
// get main categry products by ID
$category = get_category(3);

if ( have_rows('slides', $category) ) : ?>

  <section class="slider">
    <div class="slider__track is-slider">

      <?php
        // Loop through slides
        while ( have_rows('slides', $category) ) :
          the_row();
        $image = get_sub_field('slides_bg', $category);
        $title = get_sub_field('slides_title', $category);
        $caption = get_sub_field('slides_text', $category); 
        $link = get_sub_field('slides_link', $category); 
        $link_text = get_sub_field('slides_link_text', $category);
      ?>

        <div class="slider__item flex flex--valign"  style="background-image: url(<?php echo esc_url($image['url']); ?>);">
          <div class="wrap hpad slider__container">
            <div class="slider__text col-sm-6 col-sm-offset-6">
              <h2 class="slider__title h1"><?php echo $title; ?></h2>
              <?php echo $caption; ?>
              <?php if ($link) : ?>
                <a class="slider__link btn btn--red" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
              <?php endif; ?>
            </div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>
  </section>
<?php endif; ?>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="products padding--bottom">

    <div class="wrap hpad">

      <div class="products__intro">
        <?php if (category_description(3) ) : ?>
          <?php echo category_description(3); ?>
        <?php endif; ?>
      </div>


      <div class="row flex flex--wrap">
        
        <div class="col-sm-2 products__filter">
          <?php 
            get_template_part('parts/sidebar'); 
          ?>
        </div>

          <div class="col-sm-10">
            <div class="row flex flex--wrap mixit">

            <?php 

              //query arguments
              $args = array(
                'posts_per_page' => -1,
                'post_type' => 'produkt'
              );
               
              $query = new WP_QUERY($args);
             ?>

            <?php if ($query->have_posts()): ?>
              <?php while ($query->have_posts()): $query->the_post(); ?>

              <?php   
                //get thumb
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'products' );
                //post img alt tag
                $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

                $cats = get_the_category();
                $cat_string = "";

                //product sales badge
                $sales_badge = get_field('product_sales_badge');

                if ($sales_badge === true) {
                  $sales_badge_class = 'products__item--sales-badge';
                }

                foreach ($cats as $cat) {
                  $cat_string .= " cat" . $cat->term_id ."";
                }
              ?>

              <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="products__item col-sm-4 <?php echo esc_attr($class); ?> <?php echo esc_attr($sales_badge_class); ?> mix <?php echo esc_attr($cat_string); ?>" itemscope itemtype="http://schema.org/Product">

                <header>
                  <?php if ($sales_badge === true)  : ?>
                    <div class="products__sales-badge">Tilbud</div>
                  <?php endif; ?>
                  <img itemprop="image" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">
                  <h2 class="products__title--archive" itemprop="name">                   
                      <?php the_title(); ?>
                  </h2>
                </header>

                <div itemprop="description">
                  <?php the_excerpt(); ?>
                </div>

              </a>

              <?php endwhile; wp_reset_postdata(); else: ?>

                <p>Ingen produkter i denne kategori fundet.</p>

            <?php endif; ?>
            </div>
        </div>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer', 'gallery'); ?>

<?php get_template_part('parts/footer'); ?>